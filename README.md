# MD FRAMEWORK LOL

## Resources

- [TDD](https://en.wikipedia.org/wiki/Test-driven_development)
- [Jest](https://jestjs.io/)
- [Git](https://git-scm.com/)
- [Markdown](https://www.markdownguide.org/)
- [Postman](https://www.postman.com/)
- [ngrok](https://ngrok.com/)
- [Node.js](https://nodejs.org/en/docs/)
- [JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript)
- [TypeScript](https://www.typescriptlang.org/docs/)
- [ethers.js](https://docs.ethers.io)
- [PostgreSQL](https://www.postgresql.org/docs/)
- [MongoDB](https://www.mongodb.com/docs/manual/)
- [Docker](https://www.docker.com/)
- [Docker Hub](https://hub.docker.com/)
- [Docker Compose](https://docs.docker.com/compose/)
- [Dockerfile](https://docs.docker.com/engine/reference/builder/)
- [Docker Compose File](https://docs.docker.com/compose/compose-file/)
- [Gitlab](https://gitlab.com/)
- [Gitlab CI](https://docs.gitlab.com/ee/ci/)

## Revision module FS nodeJS

fs.readFile(path, options, callback); lire
fs.readFileSync(path, options); Les méthodes Sync bloquent lire de façon synchone c'est valable pour tout les méthodes
fs.writeFile(file, data, options, callback); écrire/remplace

fs.appendFile(file, data, options, callback); ecrire/ajouter du contenu
fs.rename(oldPath, newPath, callback); renommer
fs.unlink(path, callback); supprimer

fs.mkdir(path, options, callback); créer dossier
fs.rmdir(path, callback); supprimer dossier
fs.readdir(path, options, callback); lire dossier

## Revision module os nodeJS

The Node.js os module can be used to get information about the computer and operating system on which a program is running.
System architecture, network interfaces, the computer’s hostname, and system uptime are a few examples of information that can be retrieved.

os.arch(): Renvoie l'architecture du processeur de l'ordinateur.
os.cpus(): Renvoie un tableau d'objets qui contiennent des informations sur chaque CPU/logique du système.
os.freemem(): Renvoie la quantité de mémoire système libre en octets.
os.totalmem(): Renvoie la quantité totale de mémoire système en octets.
os.homedir(): Renvoie le répertoire personnel de l'utilisateur courant.
os.hostname(): Renvoie le nom de l'hôte de la machine.
os.networkInterfaces(): Renvoie un objet contenant des informations sur les interfaces réseau disponibles.
os.platform(): Renvoie le nom de la plateforme du système d'exploitation.
os.release(): Renvoie la version du système d'exploitation.
os.tmpdir(): Renvoie le répertoire temporaire du système.
os.type(): Renvoie le nom du système d'exploitation.
os.uptime(): Renvoie le nombre de secondes écoulées depuis la dernière mise sous tension du système.
os.userInfo([options]): Renvoie un objet contenant des informations sur l'utilisateur courant.

## Revision module path

path.basename(path[, ext]): Retourne le nom du fichier à partir du chemin spécifié.
path.dirname(path): Retourne le répertoire parent du chemin spécifié.
path.extname(path): Retourne l'extension du fichier à partir du chemin spécifié.
path.isAbsolute(path): Vérifie si le chemin spécifié est absolu.
path.join([...paths]): Joint les différents segments de chemin pour créer un chemin complet.
path.normalize(path): Normalise le chemin spécifié en supprimant les séquences de chemin redondantes.
path.parse(path): Analyse le chemin spécifié et retourne un objet contenant les composants du chemin.
path.resolve([...paths]): Résout les chemins spécifiés en une seule chaîne de chemin absolue.
path.sep: Propriété qui représente le séparateur de chemin spécifique au système d'exploitation.
path.delimiter: Propriété qui représente le délimiteur utilisé dans la variable d'environnement PATH.

## timer Module

setTimeout(), setInterval(), and setImmediate().

## Revision module crypto

Génération de hachages :

-crypto.createHash(algorithm) : Crée un objet de hachage pour l'algorithme spécifié.
-hash.update(data[, inputEncoding]) : Met à jour le hachage avec les données spécifiées.
-hash.digest([encoding]) : Récupère la valeur de hachage finale sous forme de chaîne de caractères ou de buffer.
Chiffrement et déchiffrement :

-crypto.createCipher(algorithm, password) : Crée un objet de chiffrement pour l'algorithme spécifié avec un mot de passe.
-crypto.createDecipher(algorithm, password) : Crée un objet de déchiffrement pour l'algorithme spécifié avec un mot de passe.
-cipher.update(data[, inputEncoding][, outputEncoding]) : Chiffre les données spécifiées.
-cipher.final([outputEncoding]) : Récupère les données chiffrées finales.
-decipher.update(data[, inputEncoding][, outputEncoding]) : Déchiffre les données spécifiées.
-decipher.final([outputEncoding]) : Récupère les données déchiffrées finales.




### Génération de clés et de paires de clés :

-crypto.generateKeyPairSync(type[, options]) : Génère une paire de clés de cryptographie de manière synchrone.
-crypto.generateKeyPair(type[, options], callback) : Génère une paire de clés de cryptographie de manière asynchrone.

Signatures numériques et vérification :

-crypto.createSign(algorithm) : Crée un objet de signature pour l'algorithme spécifié.
-sign.update(data[, inputEncoding]) : Met à jour la signature avec les données spécifiées.
-sign.sign(privateKey[, outputEncoding]) : Récupère la signature finale.
-crypto.createVerify(algorithm) : Crée un objet de vérification pour l'algorithme spécifié.
-verify.update(data[, inputEncoding]) : Met à jour la vérification avec les données spécifiées.
-verify.verify(publicKey, signature[, signatureEncoding]) : Vérifie la signature.

### exemple:

const fileBuffer = fs.readFileSync(filePath);
const hashSum = crypto.createHash('sha256');
hashSum.update(fileBuffer);
const hex = hashSum.digest('hex');

