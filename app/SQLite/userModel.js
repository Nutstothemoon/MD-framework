const { DataTypes, Model } = require('sequelize');
const sequelizeSQL = require('./sequelizeSQL');

class UserSQLite extends Model {}

UserSQLite.init(
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    sequelize: sequelizeSQL,
    modelName: 'UserSQLite',
    tableName: 'users',
  },
);

module.exports = UserSQLite;
