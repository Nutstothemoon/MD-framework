const Sequelize = require('sequelize');

const sequelizeSQL = new Sequelize({
  dialect: 'sqlite',
  storage: 'path/to/database.sqlite',
  logging: false,
});

sequelizeSQL
  .authenticate()
  .then(() => {
    console.log(`SQLite :: connection established`);
  })
  .catch((error) => {
    console.error('SQLite connection error:', error);
  });

module.exports = sequelizeSQL;
