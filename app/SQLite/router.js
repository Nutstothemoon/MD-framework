const express = require('express');
const SQLiteRouter = express.Router();
const userController = require('./userController');

// Route pour créer un utilisateur
SQLiteRouter.post('/users', userController.createUser);

// Route pour récupérer tous les utilisateurs
SQLiteRouter.get('/users', userController.getUsers);

// Route pour mettre à jour un utilisateur
SQLiteRouter.put('/users/:id', userController.updateUser);

// Route pour supprimer un utilisateur
SQLiteRouter.delete('/users/:id', userController.deleteUser);

module.exports = SQLiteRouter;