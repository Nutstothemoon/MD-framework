const express = require('express');
const MONGOrouter = express.Router();
const userController = require('./userController');

// Route pour créer un utilisateur
MONGOrouter.post('/users', userController.createUser);

// Route pour récupérer tous les utilisateurs
MONGOrouter.get('/users', userController.getUsers);

// Route pour mettre à jour un utilisateur
MONGOrouter.put('/users/:id', userController.updateUser);

// Route pour supprimer un utilisateur
MONGOrouter.delete('/users/:id', userController.deleteUser);

module.exports = MONGOrouter;
