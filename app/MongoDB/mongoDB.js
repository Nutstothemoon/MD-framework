const mongoose = require('mongoose');

require('dotenv').config();

const optionsConnection = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
};

const makeNewConnection = (uri) => {
  const db = mongoose.createConnection(uri, optionsConnection);

  db.on('error', function (error) {
    console.log(`MongoDB :: connection ${this.name} ${JSON.stringify(error)}`);
    db.close().catch(() =>
      console.log(`MongoDB :: failed to close connection ${this.name}`),
    );
  });

  db.on('connected', function () {
    mongoose.set('debug', function (col, method, query, doc) {
      console.log(
        `MongoDB :: ${this.conn.name} ${col}.${method}(${JSON.stringify(
          query,
        )},${JSON.stringify(doc)})`,
      );
    });
    console.log(`MongoDB :: connected to ${this.name}`);
  });

  db.on('disconnected', function () {
    console.log(`MongoDB :: disconnected to ${this.name}`);
  });

  return db;
};

const mongo = makeNewConnection(process.env.MONGO_URI);

const connectionDB = {
  mongo,
};

module.exports = connectionDB;
