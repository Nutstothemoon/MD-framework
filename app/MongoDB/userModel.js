const mongoose = require('mongoose');
const connectionDB = require('./mongoDB');

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
        // Définir d'autres colonnes de votre table 'user' ici
        
});

const User = connectionDB.mongo.model('User', userSchema);

module.exports = User;
