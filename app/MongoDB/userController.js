const User = require("./userModel");

// Méthode pour créer un utilisateur
const createUser = async (req, res) => {
  try {
    const { name, email, password } = req.body;

    // Création de l'utilisateur dans la base de données
    const user = new User({ name, email, password });
    await user.save();

    res.status(201).json({ user });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

// Méthode pour récupérer tous les utilisateurs
const getUsers = async (req, res) => {
  try {
    // Récupération de tous les utilisateurs depuis la base de données
    const users = await User.find();

    res.status(200).json({ users });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

// Méthode pour mettre à jour un utilisateur
const updateUser = async (req, res) => {
  try {
    const { id } = req.params;
    const { name, email, password } = req.body;

    // Recherche de l'utilisateur dans la base de données
    const user = await User.findById(id);

    if (!user) {
      return res.status(404).json({ error: "User not found" });
    }

    // Mise à jour des attributs de l'utilisateur
    user.name = name;
    user.email = email;
    user.password = password;

    // Sauvegarde des modifications dans la base de données
    await user.save();

    res.status(200).json({ user });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

// Méthode pour supprimer un utilisateur
const deleteUser = async (req, res) => {
  try {
    const { id } = req.params;

    // Suppression de l'utilisateur dans la base de données
    await User.findByIdAndDelete(id);

    res.status(200).json({ message: "User deleted" });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

module.exports = {
  createUser,
  getUsers,
  updateUser,
  deleteUser,
};
