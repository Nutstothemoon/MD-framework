const User = require('./userModel');
const sequelize = require('./sequelizePg');
// Méthode pour créer un utilisateur
const createUser = async (req, res) => {
  try {
    // Synchronisation de la base de données avec les modèles
    await sequelize.sync();
    const { name, email, password } = req.body;

    // Création de l'utilisateur dans la base de données
    const user = await User.create({ name, email, password });

    res.status(201).json({ user });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

// Méthode pour récupérer tous les utilisateurs
const getUsers = async (req, res) => {
  try {
    // Synchronisation de la base de données avec les modèles
    await sequelize.sync();
    // Récupération de tous les utilisateurs depuis la base de données
    const users = await User.findAll();

    res.status(200).json({ users });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

// Méthode pour mettre à jour un utilisateur
const updateUser = async (req, res) => {
  try {
    const { id } = req.params;
    const { name, email, password } = req.body;
    // Synchronisation de la base de données avec les modèles
    await sequelize.sync();
    // Recherche de l'utilisateur dans la base de données
    const user = await User.findByPk(id);

    if (!user) {
      return res.status(404).json({ error: 'User not found' });
    }

    // Mise à jour des attributs de l'utilisateur
    user.name = name;
    user.email = email;
    user.password = password;

    // Sauvegarde des modifications dans la base de données
    await user.save();

    res.status(200).json({ user });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

// Méthode pour supprimer un utilisateur
const deleteUser = async (req, res) => {
  try {
    const { id } = req.params;

    // Synchronisation de la base de données avec les modèles
    await sequelize.sync();

    // Suppression de l'utilisateur dans la base de données
    await User.destroy({ where: { id } });

    res.status(200).json({ message: 'User deleted' });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

module.exports = {
  createUser,
  getUsers,
  updateUser,
  deleteUser,
};
