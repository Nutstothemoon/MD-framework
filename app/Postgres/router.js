const express = require('express');
const POSTGRESrouter = express.Router();
const userController = require('./userController');

// Route pour créer un utilisateur
POSTGRESrouter.post('/users', userController.createUser);

// Route pour récupérer tous les utilisateurs
POSTGRESrouter.get('/users', userController.getUsers);

// Route pour mettre à jour un utilisateur
POSTGRESrouter.put('/users/:id', userController.updateUser);

// Route pour supprimer un utilisateur
POSTGRESrouter.delete('/users/:id', userController.deleteUser);

module.exports = POSTGRESrouter;