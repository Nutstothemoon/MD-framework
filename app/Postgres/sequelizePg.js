require('dotenv').config();
const Sequelize = require('sequelize');

const sequelizePg = new Sequelize(
  process.env.PGDATABASE,
  process.env.PGUSER,
  process.env.PGPASSWORD,
  {
    host: process.env.PGHOST,
    dialect: 'postgres',
    logging: false,
  },
);

sequelizePg
  .authenticate()
  .then(() => {
    console.log(`PostgreSQL :: connection to ${process.env.PGDATABASE}`);
  })
  .catch((error) => {
    console.error('PostgreSQL connection error:', error);
  });

module.exports = sequelizePg;
