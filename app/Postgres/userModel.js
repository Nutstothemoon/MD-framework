const { Model, DataTypes } = require('sequelize');
const sequelizePg = require('./sequelizePg');

class User extends Model {
  static init() {
    return super.init(
      {
        image_path: {
          type: DataTypes.STRING,
          allowNull: true,
        },
        // Définir d'autres colonnes de votre table 'user' ici
      },
      {
        sequelize: sequelizePg,
        modelName: 'User',
        tableName: 'user',
        timestamps: false,
      },
    );
  }

  // Ajoutez des associations, des méthodes de requête personnalisées, etc. ici
}

User.init();

module.exports = User;
