const cors = require('cors');
const express = require('express');
require('dotenv').config();
const app = express();
const port = process.env.PORT || 3001;
const bodyParser = require('body-parser');
const MONGOrouter = require('./app/MongoDB/router');
const POSTGRESrouter = require('./app/Postgres/router');
const SQLiteRouter = require('./app/SQLite/router');

// server initialization

app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.json());
app.use(cors());
app.enable('trust proxy');

// REDIRECTION ROUTER

app.use('/sqlite', SQLiteRouter);
app.use('/postgres', POSTGRESrouter);
app.use('/mongo', MONGOrouter);


app.listen(port, () => {
  console.log(`Example app listening on http://localhost:${port}`);
});

module.exports = app;
