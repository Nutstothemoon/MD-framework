package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"
)

func main() {
	repoURL := "https://github.com/your/repo.git" 

	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Voulez-vous cloner tous les dossiers du dépôt ? (Oui/Non): ")
	answer, _ := reader.ReadString('\n')

	shouldCloneAll := strings.HasPrefix(strings.ToLower(answer), "o")

	// Dossiers à cloner
	foldersToClone := []string{"folder1", "folder2", "folder3"}

	if shouldCloneAll {
		cloneRepo(repoURL)
	} else {
		for _, folder := range foldersToClone {
			cloneFolder(repoURL, folder)
		}
	}
}

func cloneRepo(repoURL string) {
	cmd := exec.Command("git", "clone", repoURL)
	err := cmd.Run()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Dépôt cloné avec succès.")
}

func cloneFolder(repoURL string, folder string) {
	cloneURL := fmt.Sprintf("%s/%s", repoURL, folder)
	cmd := exec.Command("git", "clone", cloneURL)
	err := cmd.Run()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Dossier %s cloné avec succès.\n", folder)
}
