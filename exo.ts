function twoSum(nums: number[], target: number): number[] {
  const hash = new Map();
  for (let i = 0; i < nums.length; i++) {
    const complement = target - nums[i];
    if (hash.has(complement)) {
      return [hash.get(complement), i];
    }
    hash.set(nums[i], i);
  }
  return [-1, -1];
}

function isPalindrome(x: number): boolean {
  let [right, left] = [0, dernierChiffre(x)];
  if (right < left) {
    if (right !== left) return false;
    right++;
    left--;
  }
}

function dernierChiffre(nombre) {
  // Obtenir la valeur absolue du nombre
  var absNombre = Math.abs(nombre);

  // Obtenir le dernier chiffre en utilisant l'opérateur de modulo
  var dernierChiffre = absNombre % 10;

  return dernierChiffre;
}
